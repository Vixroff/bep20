import os
import re
import time
import random

import requests
import seleniumwire.undetected_chromedriver as uc
from selenium.webdriver.common.by import By
from web3 import Web3
from web3.middleware import construct_sign_and_send_raw_middleware, geth_poa_middleware

from utils import get_proxies, FailException, parse_cli_args

FAUSET_URL = 'https://testnet.bnbchain.org/faucet-smart'
PROVIDER_URL = 'https://bsc-testnet.publicnode.com'


class Worker:

    def __init__(
        self,
        proxy: str,
        target_wallet: str,
        token: str
    ):
        self.proxy = proxy
        self.target_wallet = target_wallet
        self.token = token

        self.w3 = Web3(Web3.HTTPProvider(PROVIDER_URL))
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.w3.eth.account.enable_unaudited_hdwallet_features()
    
    def create_wallet(self):
        """
        Создание кошелька и набора ключевых фраз.
        """
        self.wallet, self.mnemonic = self.w3.eth.account.create_with_mnemonic()
        print(f'Создан кошелек {self.wallet.address}')
    
    def get_bnb(self):
        """
        Скрипт с работой Селениума.
        Прохождение капчи и отправление BNB на self.wallet.address.

        Ошибки: 
        - Exeption: в случае, если rucaptcha не смогла обработать капчу.
        """
        options = uc.ChromeOptions()
        options.add_argument('--headless')
        options.accept_insecure_certs = True
        proxy_options = {
            'proxy': {
                'http': f"http://{self.proxy}",
                'https': f"http://{self.proxy}"
            }
        }

        with uc.Chrome(
            options=options,
            seleniumwire_options=proxy_options,
        ) as browser:
            print('Парсер начал работу')
            browser.get(FAUSET_URL)

            iframe = browser.find_element(By.TAG_NAME, 'iframe')
            sitekey = re.findall(r'sitekey=(.*?)&', iframe.get_attribute('src'))[0]

            response_in = requests.get(
                    'http://rucaptcha.com/in.php',
                    params={
                        'key': self.token,
                        'method': 'hcaptcha',
                        'pageurl': FAUSET_URL,
                        'sitekey': sitekey,
                        'json': 1
                    }
                ).json()

            ready = False         
            while not ready:
                print('Ожидание решения капчи...')
                time.sleep(5)
                response_res = requests.get(
                    'http://rucaptcha.com/res.php',
                    params={
                        'key': self.token,
                        'action': 'get',
                        'id': response_in['request'],
                        'json': 1
                    }
                ).json()
                if response_res['request'] == "ERROR_CAPTCHA_UNSOLVABLE":
                    raise FailException('Не удалось решить капчу.')
                elif not response_res['request'] == 'CAPCHA_NOT_READY':
                    print('Капча пройдена успешно')
                    ready = True
                    captcha_key = response_res['request']
            
            browser.execute_script(f"assignCallback(\"{captcha_key}\")")
            browser.find_element(By.ID, 'url').send_keys(self.wallet.address)
            browser.find_element(By.XPATH, '//button[contains(text(),"Give me BNB")]').click()
            browser.find_element(By.XPATH, '//a[text()="0.1 BNB"]').click()
            print('Парсер отработал.')
            # browser.quit()
        browser.__del__()
    
    def check_parser_result(self, count=3):
        """
        Проверка поступления 0.1 BNB из фаусета на созданный кошелек.
        """
        if count == 0:
            raise FailException('Поступление BNB после парсинга не произошло.')
        balance = self.w3.eth.get_balance(self.wallet.address)
        if balance:
            print(f'Поступление произошло. Баланс: {balance}')
        else:
            print(f'Ожидание поступления...')
            time.sleep(2)
            self.check_parser_result(count=count-1)
        
    def send_crypto_to_target(self):
        """
        Метод отправляет криптовалюту на указанный checksum адрес .
        """
        self.w3.middleware_onion.add(construct_sign_and_send_raw_middleware(self.wallet))
        try:
            tx_hash = self.w3.eth.send_transaction({
                "from": self.wallet.address,
                "to": self.target_wallet,
                "value": self.w3.to_wei(0.09, 'ether'),
                "gas": 200000,
                "gasPrice": self.w3.eth.gas_price
            })
        except Exception as e:
            raise FailException(f'Отправление на основной кошелек не удалось. {e}')
        else:
            print('BNB успешно отправлен на указанный кошелек.')
    
    def do(self):
        """
        Главная задача.
        """
        self.create_wallet()
        self.get_bnb()
        self.check_parser_result()
        self.send_crypto_to_target()


def main(
    proxies_path,
    main_account,
    token
):
    proxies = get_proxies(proxies_path)

    for index, proxy in enumerate(proxies, 1):
        print(f'Задача: {index} запущена. Прокси: {proxy}')
        task = Worker(
            proxy,
            main_account,
            token
        )
        try:
            task.do()
        except FailException as e:
            print(f'Возникла ошибка: {e}')
        finally:
            print(f'Задача {index} завершена.')


if __name__ == "__main__":
    args = parse_cli_args()
    main(
        args.proxy,
        args.main_account,
        args.captcha_token
    )
