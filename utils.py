import os
import argparse


class FailException(Exception):
    pass


def handle_proxy(proxy):
    parts = proxy.split(':')
    return f'{parts[2]}:{parts[3]}@{parts[0]}:{parts[1]}'


def get_proxies(filepath):
    proxies = []
    with open(filepath, 'r') as f:
        for proxy in f.readlines():
            proxies.append(proxy.strip())
    return list(map(handle_proxy, proxies))


def parse_cli_args():
    parser = argparse.ArgumentParser(prog='python -m main')
    parser.add_argument(
        '-p', '--proxy',
        default=os.path.join(os.path.dirname(__file__), 'proxies.txt'),
        help='абсолютный путь к файлу с прокси данными.'
    )
    parser.add_argument(
        '--main-account',
        required=True,
        help='чексум адрес целевого кошелька.',
    )
    parser.add_argument(
        '-t', '--captcha-token',
        required=True,
        help='апи токен rucaptcha',
    )
    args = parser.parse_args()
    return args
